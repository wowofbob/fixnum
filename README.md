# README #

This repo contains a module `FixNum.hs` which provides a single function `fixNum`. It parses a string representing a number and fix it by doing the following:

* replaces hyphen-minus to ordinary `'-'`
* normalizes zeroes
* replaces comma `','` to dot `'.'`

Examples of work:

      0000000 -> 0
      0000001 -> 1
      
      - 00000 -> -0
      - 00001 -> -1
      
      0000,01   -> 0.01
      0001.01   -> 1.01
      0001.0100 -> 1.01
      
      - 97,37 -> -937.37
      
      –5,05   -> -5.05 ('–' == '\8211')
