module Auxiliary where

import Data.Char

isZero :: Char -> Bool
isZero = (=='0')

isMinus :: Char -> Bool
isMinus c = c == '-' || c == '\8211' -- hyphen-minus
 
{-
  NumDot is either comma or dot.
  Number `a,b` and `a.b` are equivalent.
-}
isNumDot :: Char -> Bool
isNumDot c = c `elem` [',', '.']

-- works like `all isDigit` but returns `False` on empty list
onlyDigits :: String -> Bool
onlyDigits xs = (not.null) xs && all isDigit xs 

{-
  tries to extract head from list;
  returns `Nothing` if list is empty.
-}
tryHead :: [a] -> Maybe a
tryHead []    = Nothing
tryHead (x:_) = Just x

{-
  applies function to value if predicate holds;
  doesn't change value if it not.
-}
applyIf :: Bool -> (a -> a) -> a -> a
applyIf p f a = if p then f a else a

{-
  isn't needed in this program
-} 
toMaybe :: [a] -> Maybe [a]
toMaybe [] = Nothing
toMaybe xs = Just xs

