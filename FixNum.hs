module FixNum (fixNum) where

import Control.Monad (guard)

import Data.Char (isSpace, isDigit)

import Auxiliary

{-
  `fixNum` parses a string representing a
    number and fixes it by doing the following:
     1) replaces hyphen to minus;
     2) normalizes zeroes
     3) replaces comma to dot;
     
    Examples:
    
      0000000 -> 0
      0000001 -> 1
      
      - 00000 -> -0
      - 00001 -> -1
      
      0000,01   -> 0.01
      0001.01   -> 1.01
      0001.0100 -> 1.01
      
      - 97,37 -> -937.37
      
      –5,05   -> -5.05
-}

fixNum :: String -> Maybe String
fixNum xs = number xs >>= return.num2str.normalize

-- Just wrapper for digits
newtype Digits = Digits { digitsToString :: String } deriving Show

{-
  It's not needed in this module, but to
  build `Digits` from `String` by hand
  you need `String` full of digits. In
  other case you'll get `Nothing`.
-} 
digits :: String -> Maybe Digits
digits xs
  | onlyDigits xs = Just (Digits xs)
  | otherwise     = Nothing

{-
  Number is either negative or positive (`isNegative`),
  it has integral part (`integralPart`) and maybe
  fractional part (`fractionalPart`) (if it's fractional number)
-}   
data Number = Number { isNegative     :: Bool
                     , integralPart   :: Digits
                     , fractionalPart :: Maybe Digits } deriving Show
{-
  Constructs `Number` from `String`.
  If given `String` is incorrect number,
  it returns `Nothing`. In hyphen-minus
  and comma may occur in string.
-}                     
number :: String -> Maybe Number
number zs =
  -- pass spaces
  let xs = dropWhile isSpace zs in 
    -- list shouldn't be empty then
    tryHead xs >>= \ x ->
      let isNeg  = isMinus x              -- check minus
          ys     = applyIf isNeg tail xs  -- pass one char if it has minus
          ys'    = dropWhile isSpace ys   -- pass spaces 
          (i, r) = span isDigit ys' in    -- get integral part
            -- it shouldn't be empty
            guard (not $ null i) >>
              -- like ordinary `span` but performs
              -- `span` when first character is numdot       
              let (f, r') = spanFrac r in
                -- rest string should be either empty or full of spaces 
                guard (all isSpace r') >> 
                  return (Number isNeg (Digits i) (toDigits f))
                where               
                  spanFrac xs = case xs of
                    y:ys | isNumDot y -> span isDigit ys
                    _                 -> ("", xs)
                  -- just puts string in `Digits` if it's not empty.
                  -- How to do it more gentle?
                  toDigits "" = Nothing
                  toDigits xs = Just $ Digits xs


{- Normalizes number by removing unnecassery zeroes
   from beginning of integral part and from the end of
   fractional part -}
normalize :: Number -> Number
normalize (Number isNeg ip mFp) =
  Number isNeg (normInt ip) (fmap normFrac mFp)
  where
    normIntStr ds = case dropWhile isZero ds of
                      "" -> "0"
                      ys -> ys
    normInt  (Digits ds) = Digits $ normIntStr ds
    normFrac (Digits ds) = 
      let ds' = reverse $ normIntStr $ reverse ds in Digits ds'
                                               
-- Renders `Number` into `String`
num2str :: Number -> String
num2str (Number isNeg dIp mFp) = minus ++ ip ++ fp 
  where
    minus
      | isNeg     = "-"
      | otherwise = ""
    ip = digitsToString dIp
    fp = case mFp of
      Nothing -> ""
      Just ds -> '.' : digitsToString ds
    
                    
                      
                      
                    
